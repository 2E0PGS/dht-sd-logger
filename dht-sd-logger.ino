 /*
DHT sensor to CSV SD card temp/humidity logger.
Inspiration from DHTSensorLib and SD example projects.

Copyright (C) <2021>  <Peter Stevenson> (2E0PGS)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "DHT.h"

// I tried to get clever and have the DHT direct into the Uno using pull down for ground and/or pull up for 5V.
// I even checked it was within the pins current draw however I didn't succeed as the SD card hat I use which also
// has ethernet onboard took every convenient pin so I was doubling up on pins without splitting the sensor out onto a breadboard.
#define DHTPIN 8

// Uncomment whatever type you're using.
#define DHTTYPE DHT11   // DHT 11 
//#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Optionally you can connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor.

// Initialize DHT sensor for normal 16mhz Arduino.
DHT dht(DHTPIN, DHTTYPE);

#include <SPI.h>
#include <SD.h>

/*
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4 (for MKRZero SD: SDCARD_SS_PIN)
*/

const int chipSelect = 4;
const String loggerFile = "datalog.csv";

void setup() {
  Serial.begin(9600); 
  //pinMode(11, OUTPUT); // Lazy hack as explained above.
  //digitalWrite(11, HIGH); // Lazy hack.
 
  dht.begin();
  
  Serial.print("Initializing SD card...");

  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
  
    // don't do anything more:
    while (1);
  }
  
  Serial.println("card initialized.");

  // Setup CSV columns. TODO: Only do it once if present in file header.
  // Use a empty string here so it starts doing implicit casting.
  String dataString = String() + "Humidity, " + "Temp, " + "Heat index"; 
  writeStringToFile(loggerFile, dataString);
  Serial.println("CSV columns created.");
}

void loop() {
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit
  float f = dht.readTemperature(true);
  
  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  // Compute heat index
  // Must send in temp in Fahrenheit!
  float hi = dht.computeHeatIndex(f, h);

  Serial.print("Humidity: "); 
  Serial.print(h);
  Serial.print(" % ");
  Serial.print("Temperature: "); 
  Serial.print(t);
  Serial.print(" °C ");
  Serial.print(f);
  Serial.print(" °F ");
  Serial.print("Heat index: ");
  Serial.print(hi);
  Serial.println(" °F");

  // Assemble the CSV string to log.
  String dataString = String(h) + ", " + String(t) + ", " + String(hi);  
  writeStringToFile(loggerFile, dataString);
  
  delay(300000); // 5 mins.
}

void writeStringToFile(String filename, String text) {
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  File dataFile = SD.open(filename, FILE_WRITE);

  // if the file is available, write to it:
  if (dataFile) {
    dataFile.println(text);
    dataFile.close();
  }
  // if the file isn't open, pop up an error:
  else {
    Serial.println("error opening: " + filename);
  }
}
